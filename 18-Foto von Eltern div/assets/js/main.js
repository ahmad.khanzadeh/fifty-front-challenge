const body=document.body;
const slides=document.querySelectorAll('.slide')
const leftBtn=document.getElementById('left')
const rightBtn=document.getElementById('right')

let activeSlide=2;


setBackgroundToBody()

//  a function which change the body background image to the background image of the slides
function setBackgroundToBody(){
    // slides is an array so we should choice one of the arrays
    body.style.backgroundImage=slides[activeSlide].style.backgroundImage
}

// a function which changes the activeSlide so the background color of body will be changed
function setActiveSlide(){
    slides.forEach(slide =>{
        slide.classList.remove('active')
        // this will remove former active class 
    })
    // add the active class to new selected slide 
    slides[activeSlide].classList.add('active')
}

// clickable arrows 

rightBtn.addEventListener('click',()=>{
    // increase the active number 
    activeSlide++
    // in case of last number approach, turn the number to zero
    if(activeSlide >slides.length -1){
        activeSlide=0
    }
    // call functions which changes the background and activeslide
    setActiveSlide()
    setBackgroundToBody()
})

leftBtn.addEventListener('click',()=>{
    // increase the active number 
    activeSlide--
    // in case of last number approach, turn the number to zero
    if(activeSlide==0){
        activeSlide=slides.length-1
    }
    // call functions which changes the background and activeslide
    setActiveSlide()
    setBackgroundToBody()
})