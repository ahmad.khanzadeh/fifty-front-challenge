const resultEl=document.getElementById('result')
const lengthEl=document.getElementById('lenght')
const uppercaseEl=document.getElementById('uppercase')
const lowercaseEl=document.getElementById('lowercase')
const numbersEl=document.getElementById('number')
const symbolsEl=document.getElementById('symbols')
const generateEl=document.getElementById('generate')
const clipboardEl=document.getElementById('clipboard')

/* uppercase function*/
function getRandomUpper(){
    // make a char based on random char number----A=65
    return String.fromCharCode(Math.floor(Math.random()*26) +65)
}

/* lowercase function*/

function getRandomLower(){
    // make a char based on random char number----a=97
    return String.fromCharCode(Math.floor(Math.random()*26) +97)
}

/* number function*/
function getRandomNumber(){
    // make a random number( it is a char so it starts from 48=0 to upper numbers)
    return String.fromCharCode(Math.floor(Math.random()*10) +48)
}
/* symbol function*/
function getRandomSymbol(){
    const symbols='!@#$%^&*(){}[]=<>/,.'
    return symbols[Math.floor(symbols.length*Math.random())]
}


// a function whic calls the 4 main function in a random way
const randomFunc={
    lower:getRandomLower,
    upper:getRandomUpper,
    number:getRandomNumber,
    symbol:getRandomSymbol
}
generateEl.addEventListener('click',() =>{
    // turn the inserted value to a number
    const length = +lengthEl.value
    console.log(lowercaseEl.check)
    // what are the selected options
    const hasLower=lowercaseEl.checked
    const hasUpper=uppercaseEl.checked
    const hasNumber=numbersEl.checked
    const hasSymbol=symbolsEl.checked


    resultEl.innerText=generatePassword(hasLower, hasUpper,hasNumber,hasSymbol, length)
})

function generatePassword(lower,upper,number,symbol,length){
    
    let generatedPassword=''
    const typesCount=lower + upper + number + symbol + length
    // const typesArr=[{lower},{upper},{number},{symbol},{length}]
    // we made this array to use a filter on it so the items which are false are detected
    // extract only the types whic are false( false is equal to 0 in this example)
    const typesArr=[{lower},{upper},{number},{symbol},{length}].filter(item => Object.values(item)[0])

    // if nothing is selected
    if(typesCount===0){
        return ''
    }
    // otherwise
    for(let i=0; i<=length; i+=typesCount){
        typesArr.forEach(type => {
            // get the wanted function based on recived selected elements
            // bist 12.00 auf 100
            const funcName=Object.keys(type)[0]
            // create the generated password
            generatedPassword += randomFunc[funcName]()
            console.log(generatedPassword)
        })
    }
    // return generated password--as much as user inserted
    const finalPassword=generatedPassword.slice(0,length)

    return finalPassword
}