const form=document.getElementById('form')
const input=document.getElementById('input')
const todosUl=document.getElementById('todos')

form.addEventListener('submit',(e)=>{
    // not to submit this form so the page will not be reload
    e.preventDefault()

     addTodo()
     //addTodo(e.target.value)
})

function addTodo(todo){
    
    let todoText=input.value 
    // save the input data in a local variable
    if(todo){
        todoText=todo.text
    }
    // in case there were a todo text, creat an <li> for it and put it in the todosUi
    if(todoText){
        const todoEl=document.createElement('li')
        // if the input had a .completed subelement(task was completed)
        if(todo && todo.completed){
            todoEl.classList.add('completed')
        }
        // add the local variable as a text to element
        todoEl.innerText=todoText
        // in case of click, toggle the class of completed + run yhr updateLS() to save data in localstorage
        todoEl.addEventListener('click',()=>{todoEl.classList.toggle('completed'); updateLS()})
        // in rightclick, remeove the task and save in in local storage
        todoEl.addEventListener('contextmenu',(e)=>{e.preventDefault(); todoEl.remove(); updateLS()})
        todosUl.appendChild(todoEl)

        // clear the input for next usage
        input.value=''

        // update local storage( add new one or when completed)
        updateLS()

    }
    
}
// we could just save or read strings from local storage---It is not an array
// localStorage.setItem('name', JSON.stringify(obj))
// JSON.parse(localStorage.getItem(obj))

/**
 * Things that I leran from this challenge:
 *      1_prevent the default form in case of submitting the form 
 *      2_form will automatically submit the data ==> when we call a function in a form( like line 9) we don't need to pass e.target.value
 *      3_we can put an addEventListener() to every single created element(like line 30 and 32)
 *      4_in order to listen for rightclick: A) add event listener to 'contextmenu' B) pass the event in function C)prevent default task
 *      5_ in every input form, clean the input value for next usage( like line 36)
 */

const todos=JSON.parse(localStorage.getItem('todos'))

// if there were some taks in localstorage, read them and make element for them
if(todos){
    todos.forEach(todo => addTodo(todo))
}

// this function will update localstorage(will write and delete)
function updateLS(){
    // select all current li ( which contains tasks) and save data in an array
    todosEl=document.querySelectorAll('li')

    const todos=[]
    todosEl.forEach(todoEl =>{
        todos.push({
            text:todoEl.innerText,
            completed:todoEl.classList.contains('completed')
        })
    })
    // now update the local storage
    localStorage.setItem('todos', JSON.stringify(todos))
}