const APIURL='https://api.github.com/users/'
const form=document.getElementById('form')
const search =document.getElementById('search')
const main =document.getElementById('main')


/* in case of a direct call */
// function getUser(username){
//     axios.get(APIURL + username).then(res =>console.log(res)).catch(err => console.log(err))
// }
/* in case of async call send username and recive information*/
async function getUser(username){
    try{
        const {data} = await  axios.get(APIURL + username)
        // change the page information by reciving datas
        createUserCard(data)
        // get the user repositories and dispaly it in the user card
        getRepos(username)
    }catch(err){
        console.log(err)
        // in case there were a 404 error, display a specific error
        if(err.response.status==404){
            createErrorCard('Wir haben nicht das Person gefunden')
        }else{
            main.innerHTML=`<h1>Wir haben ein Problem!</h1>`
        }
        // display an error in main page
        
    }

}

/* In case of pressing enter after any search in serachbox, the following code will fire getUser() function */
form.addEventListener('submit',(e)=>{
   // to stop refreshing the page 
    e.preventDefault()
    // what is the searched username
    const searchedUser=search.value
    // if any user was searched 
    if(searchedUser){
        // call the get user funciton 
        getUser(searchedUser)
        // clear the search value
        searchedUser.value=''
    }
})

/* making a new card based on recived data from github*/
function createUserCard(user){
    const cardHTML=`
    <div class="card">
       <img src="${user.avatar_url}" alt="avatar" class="${user.name}" >

        <div class="user-info">
           <h2>${user.name}</h2>
           <p>${user.bio}</p>
           <ul>
                 <li>${user.followers} <strong>Followers </strong></li>
                 <li>${user.following} <strong>Following </strong></li>
                 <li>${user.public_repos} <strong>Repos </strong></li>
            </ul>

          <div id="repos">
                <a href="#" class="repo">Repo 1</a>
                <a href="#" class="repo">Repo 2</a>
                <a href="#" class="repo">Repo 3</a>
          </div>
       </div>
   </div>
    `
    // append created elemnt into <main> element in html
    main.innerHTML=cardHTML
}
/* to make an error card the following function will be running*/
function createErrorCard(theMessage){
    const cardHTML=`
    <div class="card">
        <h1>${theMessage}</h1>
    </div>
    `
    // append created element to main element
    main.innerHTML=cardHTML
}


/* adding repositories to each card

it is async , since we need to wait for response*/

async function getRepos(username){
    try{
        const {data}=await axios.get(APIURL + username + '/repos')
        // based on github repository, we can recive repository data based on create date
        // const {data}=await axios.get(APIURL + username + '/repos?sort=created')
        // add recived data to the card
        addReposToCard(data)
    }catch{
        createErrorCard('Das Repository ist nicht Da')
    }
}
/* add repository information to each single card */
function addReposToCard(repos){
    const reposEl=document.getElementById('repos')
    // there might be many repositories, make <a> and put repository inside it
    // this may just show first 10 repositories
    // repos.slice(0,10).forEach
    repos.forEach(singleRepo => {
        const singleRepoElemnt=document.createElement('a')
        singleRepoElemnt.classList.add('repo')
        singleRepoElemnt.href=singleRepo.html_url
        singleRepoElemnt.target='_blank'
        singleRepoElemnt.innerText= repo.name

        // add the created <a > tag to the repo element in HTML
        reposEl.appendChild(singleRepoElemnt)
    })

}
