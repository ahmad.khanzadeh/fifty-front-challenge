const loadText=document.querySelector('.loading-text');
const bg=document.querySelector('.bg');

let load=0;
let int=setInterval(bluring,50)

function bluring(){
    load++;
    if(load >=100){
        // to stop counting < stop the set interval function
        clearInterval(int);
    }
    // display text on the page
    loadText.innerText=`${load}%`
    // adding blurry effect on the main image and text( found on stack overflow)
    loadText.style.opacity=scale(load,0,100,1,0);
    bg.style.filter=`blur(${scale(load,0,100,30,0)}px)`

}

const scale=(num,in_min,in_max,out_min,out_max)=>{
    return(num-in_min)*(out_max - out_min)/ (in_max - in_min) + out_min;
}

