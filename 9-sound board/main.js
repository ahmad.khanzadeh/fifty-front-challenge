const sounds=['Divoone','Ghalbam','Jazab','Shomal','Zakhm'];

sounds.forEach(sound =>{
    const btn=document.createElement('button');
    btn.classList.add('btn');

    btn.innerText=sound;
    // play every single sound 
    btn.addEventListener('click',()=>{
        // stop older songs
        stopAllSongs();
        // play the song 
        document.getElementById(sound).play()
    })

    document.getElementById('buttons').appendChild(btn);
})

function stopAllSongs(){
    sounds.forEach(sound =>{
        const song=document.getElementById(sound)

        song.pause()
        song.currentTime=0;
    })
}