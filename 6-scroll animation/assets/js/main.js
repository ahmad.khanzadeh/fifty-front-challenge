const boxes=document.querySelectorAll('.boxes');

window.addEventListener('scroll',()=>{
    // to measur the height of the page 
    const triggerBottom=window.innerHeight/5 *4 ;

    boxes.forEach(box=>{
        // to measure the position of each blue box
        const boxTop=box.getBoundingClientRect().top;

        // in case of having sufficient space, show class will be added 
        if(boxTop <triggerBottom){
            box.classList.add('show')
        }else{
            box.classList.remove('show');
        }
    })
})
