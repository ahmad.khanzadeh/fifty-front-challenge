const results=document.getElementById('results')
const filter=document.getElementById('filter')
// collect alll recived data in aray
const listItems=[]
/**
 * Attentions: 
 * since getData() is an async function, it should be called
 * before the function demonstration( I call function in line 11 and function is at line 19) ---in reverse case, no fucntion would be called
 */

getData()
// a function which calles an online api ---> async

// in case user types anything in filter box, the following function will make a filter on  recived data 
filter.addEventListener('input', (e)=>{filterTheData(e.target.value)})


// getData() will recive infos and display them in matin box
async function getData(){
    // a http request --> wait for answer
    const res= await fetch('https://randomuser.me/api?results=20')

    const data= await res.json()
    // const data=res.json() ---> will also recive many other informations about connection and not just results
   
    data.results.forEach(singleUser => {
        // create an <li> for each user and push it to listItems array( we will display it later)
        const li=document.createElement('li')
        // push recived data to listItems[] in order to make it awailable for next searches in line 45
        listItems.push(li)
        li.innerHTML=`
            <img src="${singleUser.picture.large}" alt="${singleUser.name.last}">
             <div class="user-info">
                <h4>${singleUser.name.first} , ${singleUser.name.last}</h4>
                <p>${singleUser.location.city} , ${singleUser.location.country}</p>
            </div>
        `
        
         results.appendChild(li)
    });
}


// the function which filters the data 
function filterTheData(seaerchedItem){
    // search the intire listItems for the searchedItem 
    listItems.forEach(item =>{
        if(item.innerText.toLowerCase().includes(seaerchedItem.toLowerCase())){
            // other element already have a 'class = hide '.  we should remove that class from this element
            item.classList.remove('hide') 
        }else{
            // add hide class to not matched items
            item.classList.add('hide')
        }
    })
}

/**
 * 
 * Things to learn from this project:
 * how to make a filter on recived data from an external api ( function at line 45 and a single line in line 30)
 * how to add a specific style to last element in an <li> list: style.css line 79
 * how to add a scrollbar inside an element: style.css line 56
 * how to fit an image to an element : style.css line 67
 */