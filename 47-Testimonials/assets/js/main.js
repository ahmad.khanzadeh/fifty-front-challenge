const testimonialsContainer=document.querySelector('.testimonials-container')
const testimonial = document.querySelector('.testimonial')
const userImage=document.querySelector('.user-image')
const username=document.querySelector('.username')
const role=document.querySelector('.role')


const testimonials=[
    {
        name:'Maximilian Osteman',
        position:'Verkaufer',
        photo:'assets/img/1.jpg',
        text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
    },
    {
        name:'Johnathan Hoffman',
        position:'Maler',
        photo:'assets/img/2.jpg',
        text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et  At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. no sea takimata sanctus est Lorem ipsum dolor sit amet.',
    },
    {
        name:'Silke Mueler',
        position:'Bankerin',
        photo:'assets/img/3.jpg',
        text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
    },
    {
        name:'Timo Seppala',
        position:'Maler',
        photo:'assets/img/4.jpg',
        text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. no sea takimata sanctus est Lorem ipsum dolor sit amet.',
    },
    {
        name:'Johnathan Hoffman',
        position:'Maler',
        photo:'assets/img/5.jpg',
        text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et  At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. no sea takimata sanctus est Lorem ipsum dolor sit amet.',
    },
]

let idx=1

function updateTestimonials(){
    // get all data at the same time 
    const {name , position, photo,text}=testimonials[idx]

    testimonial.innerText=text
    userImage.src=photo
    username.innerHTML=name
    role.innerHTML=position

    if(idx<testimonials.length-1){
        idx++
    }else{
        idx=0
    }
    // setTimeout(updateTestimonials, 10000)
}

 setInterval(updateTestimonials,10000)
