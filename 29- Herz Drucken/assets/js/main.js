const loveMe=document.querySelector('.loveMe')
const times=document.querySelector('#small')

let clickTime=0 
let timesClicked=0

// for a simpler idea, we could use dblclick rather than click----no clickTime would be needed
loveMe.addEventListener('click',(e)=>{
    if(clickTime===0){
        // this is the first click
        clickTime=new Date().getTime()
    }else{
        // it is the second click time--less than 0.8s is double click
        if((new Date().getTime()-clickTime)<800){
            // that was a double click-call the like animation and reset the time 
            clickTime=0
            // this is the like function 
            createHeart(e)
        }else{
            // there was just two click on the image-set the click time to current time 
            clickTime= new Date().getTime()
        }
    }
})


// createHeart is an arrow function-since we need 'this' to notify the destination
const createHeart = (e)=>{
    const heart=document.createElement('i')
    heart.classList.add('fa')
    heart.classList.add('fa-heart')
    heart.classList.add('super-z-index')

    // find the clicked position
    const x=e.clientX
    const y=e.clientY
    // the image position on its  own
    const leftOffset=e.target.offsetLeft
    const topOffset=e.target.offsetTop
    // relative clicked position 
    const xInside=x-leftOffset
    const yInside=y-topOffset

   console.log(xInside,yInside)
    //set the top and left of created heart element to xInside & yInside
    heart.style.top=`${yInside}px`
    heart.style.left=`${xInside}px` 

    
    loveMe.appendChild(heart)
    // increase the clicked time counter 
    times.innerHTML=++timesClicked

    // ===================================
    // to remove the created heart element after 5 second
    // ===================================

    setTimeout(()=>{heart.remove()},2000)
}