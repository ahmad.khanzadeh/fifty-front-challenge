const nums=document.querySelectorAll('.nums span')
const counter=document.querySelector('.counter')
const finalMessage=document.querySelector('.final')
const replay=document.querySelector('#replay')

runAnimation()

function runAnimation(){
    nums.forEach((num,idx)=>{
        const nextToLast=nums.length -1

        num.addEventListener('animationend',(e)=>{
            if(e.animationName ==='myGoIn' && idx!== nextToLast){
                // preper the current number to leave the screen
                console.log('in was touched ')
                num.classList.remove('in')
                num.classList.add('out')
            }else if(e.animationName==='myGoOut' && num.nextElementSibling){
                // display next number
                console.log('out was touched ')
                num.nextElementSibling.classList.add('in')
            }
            else{
                // all numbers are displayed--display final text 
                counter.classList.add('hide')
                finalMessage.classList.add('show')
            }
        })
    })
}

// reseting counter function 
function resetDOM(){
    counter.classList.remove('hide')
    finalMessage.classList.remove('show')


    nums.forEach((num) =>{
        num.classList.value=''
    })
    nums[0].classList.add('in')
}

replay.addEventListener('click',()=>{
    resetDOM()
    runAnimation()
})