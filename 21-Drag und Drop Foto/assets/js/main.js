const fill=document.querySelector('.fill')
const empties=document.querySelectorAll('.empty')

// filled squer 
fill.addEventListener('dragstart',dragStart)
fill.addEventListener('dragend',dragEnd)

// empty white squeres
for(const empty of empties){
    empty.addEventListener('dragover',dragOver)
    empty.addEventListener('dragenter',dragEnter)
    empty.addEventListener('dragleave',dragLeave)
    empty.addEventListener('drop',dragDrop)

    // bist 5:30
}
// functions
function dragStart(){
    console.log('Drag start')
    // add a hold class to the image
    // this in this function will represent the image ( <div class="fill">)
    // this.className += 'hold'
    // Start point should be clean(no image content) but not just when we drag it( just run the code after all runs--zero priority)
    //  setTimeout(()=>this.className = 'invisible',0)
    
}
function dragEnd(){
    console.log('Drag end')
    // image should be hold in the destination squere
    // this.className='fill'
    
}
function dragOver(e){
    console.log('Drag over')
    // this js function has a default which makes the drag and drop reversible
    // (eventhough the user drag sth to swhr, this will makes everything the same as initail condition)
    // we stop the submision
    e.preventDefault()
}
function dragEnter(e){
    console.log('Drag Enter')
    // this js function has a default which makes the drag and drop reversible
    // (eventhough the user drag sth to swhr, this will makes everything the same as initail condition)
    // we stop the submision 
    e.preventDefault()
    // add a style to the reciving process
    // this.className +='hovered'
    this.classname.add('hovered')
}
function dragLeave(){
    console.log('Drag leave')
    // this.className -= 'hovered'
    this.classlist.remove('hovered')
    this.classname.add('empty')
}
function dragDrop(){
    console.log('Drag drop')
    // original squer should be empty while destination shoul contain image
    this.className='empty'
    // append a childe into destination element
    this.append(fill)
}