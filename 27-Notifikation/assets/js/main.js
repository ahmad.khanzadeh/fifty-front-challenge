const button=document.getElementById('btn')
const toasts=document.getElementById('toasts')

const message=[
    'Ersten Information',
    'Zweiten Information',
    'driten Information',
    'Vierten Information',
]
const types=['info','success','error']

// simple function for how it works 
// button.addEventListener('click',()=>createNotification())

// function createNotification(){
//     const notif=document.createElement('div')
//     notif.classList.add('toast')
//     notif.innerHTML=getRandomMessage()
//     toasts.appendChild(notif)

//     setTimeout(()=>{notif.remove()},3000)

// }

button.addEventListener('click',()=>createNotification())

function createNotification(message=null, type=null){
    const notif=document.createElement('div')
    notif.classList.add('toast')
    notif.classList.add(type ? type: getRandomType())
    notif.innerText=message ? message : getRandomMessage()
    toasts.appendChild(notif)

    setTimeout(()=>{notif.remove()},3000)

}

// select a random message 
function getRandomMessage(){
     return message[Math.floor(Math.random()*message.length)]
    
}
// select a random type
function getRandomType(){
    return types[Math.floor(Math.random()*types.length)]
}