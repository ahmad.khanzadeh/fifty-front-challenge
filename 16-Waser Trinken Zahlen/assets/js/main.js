const smallCups=document.querySelectorAll('.cup-small')
const liters=document.getElementById('liters')
const percentage=document.getElementById('percentage')
const remained=document.getElementById('remained')


// to add style to big main cup 
updateBigCup()

smallCups.forEach((cup,idx)=>{
    cup.addEventListener('click',()=>highlightedCups(idx))
})
 

function highlightedCups(idx){
    // add full class to small cups in order to make them entirely blue
    // in case user click the last full cup, decrease the amount by just one cup
    if(smallCups[idx].classList.contains('full') && !smallCups[idx].nextElementSibling.classList.contains('full')){
            idx--
    }
    smallCups.forEach((cup,idx2)=>{
        // all cups less then this number will recive a full class 
        // idx2 => index of counter in the smallCups array
        // idx => passed index ( the clicked cup indicator)
        if(idx2 <=idx){
            cup.classList.add('full')
        }else{
            cup.classList.remove('full')
        }
    })
    // to fill the main big cup
    updateBigCup()
}

function updateBigCup(){
    // clicked cups will have a class full- count them by selecting all elements with class .full
    const fullCups=document.querySelectorAll('.cup-small.full').length

    const totalCups=smallCups.length
    // if no cups is selected, do not display the text and % in big cup
    if(fullCups===0){
        percentage.style.visibility='hidden'
        percentage.style.height=0
    }else{
        // else there should be style of partial full cup and different percentage
        percentage.style.visibility='visible'
        percentage.style.height=`${fullCups/totalCups *330}px`
        percentage.innerText=`${fullCups/totalCups *100}%`
    }
    // in case the total cups are clicked, upper white space will be disappear
    if(fullCups === totalCups){
        remained.style.visibility='hidden'
        remained.style.height=0
    }else{
        // the remainded class will be visible and remained liters will be counted and displayed
        remained.style.visibility='visible'
        liters.innerText=`${2-(250 * fullCups/1000)} Liters`
    }
}