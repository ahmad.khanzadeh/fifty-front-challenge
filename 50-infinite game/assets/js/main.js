const screens=document.querySelectorAll('.screen');
const choose_insect_btns=document.querySelectorAll('.choose-insect-btn');
const start_btn=document.getElementById('start-btn');
const timeEl=document.getElementById('time')
const scoreEl=document.getElementById('score')
const message=document.getElementById('message')
const game_container=document.getElementById('game-container')

let seconds=0
let score=0
let selected_insect={}

// display insect select screen
start_btn.addEventListener('click',()=>screens[0].classList.add('up'))

// put the seleceted image as a source
choose_insect_btns.forEach(btn=>{
    // 
    btn.addEventListener('click',()=>{
        const img=btn.querySelector('img')
        const src=img.getAttribute('src')
        const alt=img.getAttribute('alt')
        selected_insect={src,alt}

        // display the game screen 
        screens[1].classList.add('up')

        // start the timer and game
        setTimeout(createInsect,1000)
        startGame()
    })
})



function createInsect(){
    // create an insect container and locate it somewhere in page
    const insect=document.createElement('div')
    insect.classList.add('insect')
    const {x,y}=getRandomLocation()
    insect.style.top=`${y}px`
    insect.style.left=`${x}px`
    // make an image(from selected image) and put it insice container
    insect.innerHTML=`<img src="${selected_insect.src}" alt="${selected_insect.alt}" style="transform: rotate(${Math.random()*360}deg)"> `
    // when insect is selected, delete it and create two new insect in a random place
    insect.addEventListener('click',()=>{catchSelectedInsect})
    
    game_container.appendChild(insect)
}

function getRandomLocation(){
    const width=window.innerWidth
    const height=window.innerHeight
    // image shoul be inside the window ==> decrease image width(200px) + add a 100px margin
    const x=Math.random()*(width-200) +100
    const y=Math.random()*(height-200) +100

    return{x,y}
}
function catchSelectedInsect(){
    console.log('Now i get it ')
    // make a function which increase score
    increaseScore()
    // add the class 'caught' to hide cautched insect just 2 seconds later
    this.classList.add('caught')
    setTimeout(()=>{this.remove()},2000)
    // add two new insects
    addInsects()
}
function addInsects(){
    setTimeout(createInsect,1000)
    setTimeout(createInsect,1500)
}
function startGame(){
    // runn a function which count date
    setInterval(increaseTime, 1000)
}
function increaseTime(){
    let m=Math.floor(seconds/60)
    let s=seconds%60

    // if seconds or minuts are les then 10, add a 0 to them 
    m=m<10?`0${m}`:m
    s=s<10?`0${s}`:s
    timeEl.innerHTML=`Time: ${m}:${s}`
    // increase second
    seconds++
}

function increaseScore(){
    score++
    scoreEl.innerHTML=`Score: ${score}`
    // if score is more then 20, display the text
    if(score>19){
        message.classList.add('visible')
    }
}