const jokeEl=document.getElementById('joke');
const jokeBtn=document.getElementById('jokeBtn');


jokeBtn.addEventListener('click',generateJoke);
generateJoke()

// USING async/await
 async function generateJoke(){
    // fetch('address' , configuration).then(select data from complete json).then(look for the sended data from server)
    config={
        headers :{
            Accept: 'application/json'
        },
    }
    // await and async was added to the name of the function because it should 
    // wait the process until data recived from the server 
    const res=await fetch('https://icanhazdadjoke.com',config)

    // extract resived data from res and it's async so add the word await before the operation
    const data= await res.json()

    jokeEl.innerHTML=data.joke
}


// USING  .then()

// function generateJoke(){
//     // fetch('address' , configuration).then(select data from complete json).then(look for the sended data from server)
//     fetch('https://icanhazdadjoke.com',{
//         headers :{
//             'Accept': 'application/json'
//         }
//     }).then((res) => res.json()).then((data) => {console.log(data);jokeEl.innerHTML=data.joke})
//     // then() because it will return a promis back
// }