const API_URL='https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=3fd2be6f0c70a2a598f084ddfb75487c&page=1'
//                the address       / version / receve popular movies(described by api) & personal api key (for my registeration on the site) & just get me the first page insted of all results
const IMG_PATH='https://image.tmbd.org/t/p/w1280'
// based on the documentation, the upper path should be added to each image address which is recived in results( each film image has a relative path and this path should be added to the first of the image address to be displayable)
const search_URL='https://api.themoviedb.org/3/search/movie?api_key=3fd2be6f0c70a2a598f084ddfb75487c&querry="'
// url for search      address         /version/search action / in movie? add api key          &send the searche name by the querry
const form=document.getElementById('form')
// access search box in here
const search=document.getElementById('search')
// access inputbox --call it search
const main=document.getElementById('main')


// get initial movies
getMovies(API_URL);


//  a function whic send request to URL--should recives smth from server=> async function
async function getMovies(url){
    const res=await fetch(url)
    // fetch data from server- wait for the responses
    const data= await res.json()
    // extract data from resived object ====> data is no in an array called results =====> console.log(data.results)
    showMovies(data.results)
    // a function which display all data whit the same shape of elements in DOM
}

// in case of submiting a new searched item in search box, the following function will send a request fo url
form.addEventListener('submit',(e)=>{
    e.preventDefault()
    // to stop reloading page

    const searchTerm=search.value;
    // if anything was entered by user, call async function getMovies()
    if(searchTerm && searchTerm!==''){
        getMovies(search_URL + searchTerm)
        // search name is no in searchTerm-- concat its value to the search_URL to recive data
        search.vaule=''
        // empty the search boc value for next searchs
    }else{
        window.location.reload()
        // relode the page for new top movie results
    }
})

// this function will display data in the DOM
function showMovies(movies){
    main.innerHTML=''
    // clean the initial template
    // separate all recived data in response and display them all 
    movies.foreach((movie)=>{
        const{title,poster_path,vote_average,overview}=movie
        // this is how we extract multiple data from the array-- names are the same as recived names

        // make the same html as template and fill it by recived data 
        const movieEl=document.createElement('div')
        movieEl.classList.add('movie')
        movieEl.innerHTML=`
        <img src="${IMG_PATH + poster_path}" alt="${title}">
            
        <div class="movie-info">
            <h3>${title}</h3>
            <span class="${getClassByRate(vote_average)}">${vote_average}</span>
        </div>
        <div class="overview">
            <h3>overview</h3>
            ${overview}
        </div>
        `


        // append the movieEl to main element
        main.appendChild(movieEl)
    })
}

// this function will chose the color for rate base on the vote_average value
function getClassByRate(voteVall){
    // the name of classes are described in style.css 
    // attention: the return of this function should be a string since it is going to be the name of class  
    if(voteVall >=8){
        return 'green'
    }else if(voteVall<=5){
        return 'red'
    }else{
        return 'orange'
    }
}
