/**
 * since only one digit should be typed in a single box, 
 * the next box should be selected automaticaly, when next digiit is clicked
 */
const codes=document.querySelectorAll('.code')

// auto focus on first element 
codes[0].focus()

// in case any digit was clicked ( from 0 to 9) go automatically to next input fild
codes.forEach((singleCode,idx) =>{
    singleCode.addEventListener('keydown', (e)=>{
        if(e.key >=0 &&e.key <=9){
            codes[idx].value=''
            // select next input field( focus on next field)
            /**
             * attention: in case we dont set a setTimeOut on this code, 
             * inserted code will be typed in the next input field 
             * since first this code changes the selected input and then 
             * typed value will be inserted
             * so 
             * do not just type :
             * codes[idx +1].focus()
             * 
             */
           setTimeout(() => {codes[idx +1].focus()},0) 
        }else if(e.key ==='Backspace'){
            // in case user pressed backspce, go back to former input field
            setTimeout(() => {codes[idx - 1].focus()},0)
        }
    })
})