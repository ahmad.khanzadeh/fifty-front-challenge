const range=document.getElementById('range')

// to find out when user changes the input value
range.addEventListener('input',(e)=>{
    // value is in the e.target.value--just turn it to a number
    const value=+e.target.value
    // the display label is just next to the input in HTNL
    const label=e.target.nextElementSibling
    label.innerHTML=value
    // to move the display box on top of input, the following line will calculate the left axis 
    const range_width=getComputedStyle(e.target).getPropertyValue('width')
    const label_width=getComputedStyle(label).getPropertyValue('width')
    // the upper values have a px on their end. first remove it from value
    const num_width=+range_width.substring(0,range_width.length -2)
    const num_label_width=+label_width.substring(0,label_width.length -2)

    // what is the min and max value
    const max=+e.target.max
    const min=+e.target.min
    // to position the box in the page
    const left=value*(num_width/ max) -num_label_width /2 
    // const left=value*(num_width/ max) -num_label_width /2 + scale(value, min,max,10,-10)

    label.style.left=`${left}px`
    // to prevent left and right jump of the box

})

// from stackoverflow question 10756313/javascrip-jquery-map-arange-of-numbers-to-another-range-of-numbers
const scale=(num,in_min,in_max, out_min, out_max)=>{
    return (num - in_min) *(out_max - out_min) / (in_max - in_min) +out_min;
}