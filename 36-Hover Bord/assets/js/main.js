const container=document.getElementById('container')
const colors=['#e74c3c','#8e44ad','#3498db','#e67e22','#2ecc71']
const SQUARES=1049

for(let i=0; i<SQUARES; i++){
    const square=document.createElement('div')
    // add square styles to created element(.squire)
    square.classList.add('squire')
    // two local functions which changes the color when mouse is in and mouse is out
    square.addEventListener('mouseenter',()=>{mySetColo(square)})
    square.addEventListener('mouseleave',()=>{myRemoveColor(square)})

    container.appendChild(square)

}


function mySetColo(ele){
    const color=myGetRandomColor()
    ele.style.background=color
    ele.style.boxShadow=`0 0 2px ${color} 0 0 10px ${color}`
}

function myRemoveColor(ele){
    ele.style.background=`#1d1d1d`
    ele.style.boxShadow=`0 0 2px #000`
}

function myGetRandomColor(){
    return colors[Math.floor(Math.random()*colors.length)]
}