const tagsEl=document.getElementById('tags');
const textarea=document.getElementById('textarea');

// check if the textarea is focused
textarea.focus()

textarea.addEventListener('keyup',(e)=>{
    // pass the entered key to a function which make span of every single entered word
    createTags(e.target.value)
    // in case of presing Enter key, selecting progress will be started ( it always must be equal to Enter)
    if(e.key=='Enter'){
        setTimeout(() =>{
            // clean the typed name 
            e.target.value=''
        },100)
        // select on of the typed word by a function 
        randomSelect();
    }
})


// the following function will put all typed words in a array and then turns them to <span> and display it to the user
function createTags(inputs){
    // split incomming inputs by comma (,) and put them in an array 
           // const allTags=inputs.split(',');
    // to clear the input data
    const tags=inputs.split(',').filter(tag=>tag.trim()!=='').map(tag =>tag.trim())

    // empty the front from the tag:
    // if we dont clean tagsEl, after each character typing, a new span will be created( to find out more, just cumment the next line)
     tagsEl.innerHTML= ' '

    // write all inputs as an <span> in the tags
    tags.forEach(tag => {
        // put the input word on a span
        const singletagEl=document.createElement('span')
        singletagEl.classList.add('tag')
        singletagEl.innerText=tag
        //  and append to the child
          tagsEl.appendChild(singletagEl)
    });
}


// the following function will select one of the options randomly
function randomSelect(){
    const times=30;

    const interval=setInterval(()=>{
        // select a random tag to add and remove its background color 
        const randomTag=pickRandomTag();
        // highligh and unhiglighting the tag
        highlightTag(randomTag);
        setTimeout(()=>{
            unHighlightTag(randomTag);
        },100)
    },100)

    // to stop random select and show the final choice
    setTimeout(()=>{
        // stop former intervals to just bold the ultimate choice
        clearInterval(interval)
        // select the ultimate selected tag and change its background color forever
        setTimeout(()=>{
            const randomTag=pickRandomTag()
            highlightTag(randomTag)
        },)
    },times*100)
}

function pickRandomTag(){
    const tags=document.querySelectorAll('.tag')
    return tags[Math.floor(Math.random()*tags.length)]
}

// styling functions
function highlightTag(tag){
    tag.classList.add('highlight')
}

function unHighlightTag(tag){
    tag.classList.remove('highlight')
}