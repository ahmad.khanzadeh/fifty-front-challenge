const hourEl=document.querySelector('.hour');
const minuteEl=document.querySelector('.minute');
const secondEl=document.querySelector('.second');
const timeEl=document.querySelector('.time');
const dateEl=document.querySelector('.date');
const toggleEl=document.querySelector('.toggle');
const HTMLEl=document.querySelector('html')


// pre selected arrays

const days=["Sonntag","Montag","Mitwoch","Dinstag","Donerstag","Freitag","Samstag"];
const months=["jan","Feb","Mar","Apr","Mai","Juni","July","Aug","Sep","Oct","Nov","Dez"];

toggleEl.addEventListener('click',()=>{
    HTMLEl.classList.toggle('dark');
})

function setTime(){
    const time=new Date();
    const month=time.getMonth();
    const date=time.getDate();
    const day=time.getDay();
    const hours=time.getHours();
    const hoursForClock= hours % 12;
    const minutes=time.getMinutes();
    const seconds=time.getSeconds();
    const ampm=hours >12 ?'PM':'AM'
    
   

    hourEl.style.transform=`translate(-50%,-100%) rotate(${scale(hoursForClock, 0, 11, 0, 360)}deg)`
    minuteEl.style.transform=`translate(-50%,-100%) rotate(${scale(minutes, 0, 59, 0, 360)}deg)`
    secondEl.style.transform=`translate(-50%,-100%) rotate(${scale(seconds, 0, 59, 0, 360)}deg)`
    // I got two lessons: 
    // 1- the const scale should be defined outside of the setTime function 
    // 2_ adding ; at the end of the style.transform will break down the code ( eventhough the original style have it!)
    timeEl.innerHTML=`${hours}:${minutes <10 ? `0${minutes}`: minutes} ${ampm}`
    // get the date
    dateEl.innerHTML=`${days[day]} ,${months[month]} <span class="circle">${date}</span>`
}
     //  a function which will extract the min, hr, second and ... in javascript 
    // from a stackoverflow sourrce code
    // StackOverflow https://stackoverflow.com/questions/10756313/javascript-jquery-map-a-range-of-numbers-to-another-range-of-numbers

    const scale=(num, in_min, in_max, out_min, out_max) =>{
        return(num-in_min)*(out_max- out_min)/(in_max-in_min) + out_min;
    }
setInterval(setTime,1000);