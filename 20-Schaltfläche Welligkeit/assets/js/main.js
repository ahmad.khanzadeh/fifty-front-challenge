const buttons=document.querySelectorAll('.ripple');

buttons.forEach(button=>{
    button.addEventListener('click',function (e){
        const x=e.clientX
        const y=e.clientY

        // to locate the button borders on it's own
        const buttonTop =e.target.offsetTop
        const buttonLeft=e.target.offsetLeft

        // to get the relative clicked position
        const xInside= x -buttonLeft
        const yInside= y - buttonTop

        // create a span element whic has the circle class(for white background) and  the clicked position
        const circle=document.createElement('span')
        circle.classList.add('circle')
        circle.style.top= yInside + 'px'
        circle.style.left=xInside + 'px'

        // appent this <span> element to this button
        // since I need "this" to point out the button, I change the eventListener from arrow function to a regular one(line 4)
        this.appendChild(circle)

        // the code will work up to line 24 but every single click will make a new span- to clear extra spans:
        setTimeout(()=>{circle.remove()},1000)

    })
})