/**
 * In case there are many options to click, Bubbling is the suitable way 
 * to listen to user selection rather than making a forEach loop and add an 
 * addEventListener() to each element
 */

const ratings= document.querySelectorAll('.rating')
const ratingsContainer= document.querySelector('.rating-container')
const sendBtn=document.querySelector('#send')
const panel=document.querySelector('#panel')
let selectedRating='Zufrieden'

ratingsContainer.addEventListener('click',(e)=>{
// panel.addEventListener('click',(e)=>{
    // just in case user clicked a ration option( which contains the class ".rating")
    if(e.target.parentNode.classList.contains('rating')){
        // a function which diselect other options
        removeActive()
        e.target.parentNode.classList.add('active')
        // select the <small> value for the selected item
        selectedRating=e.target.nextElementSibling.innerHTML
    }
})

// Button functinality
sendBtn.addEventListener('click',(e)=>{
    panel.innerHTML=`<i class="fas fa-heart"></i>
    <strong>Danke schön!</strong>
    <br>
    <strong>Ihre Wahl ist : ${selectedRating}</strong>
    <p> Wir wollen deine Antwort für unsere Kundendienst vervenden!</p>`
})
// a function which removes all former active classes--fired in line 16 befor new selection
function removeActive(){
    for(let i=0; i<ratings.length; i++){
        ratings[i].classList.remove('active')
    }
}
/**
 * Thing to learn from this project:
 *      1-How to listen to childs element from a parent element: line 13  and line 16
 *      2_parentNode useage: line 16
 *      3- select sth just next to clicked element: line 21
*/