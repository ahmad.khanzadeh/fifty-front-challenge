const textEl=document.getElementById('text')
const speedEl=document.getElementById('speed')
const text="Sehr geherte Damen und Herren..."
let idx=1
let speed = 300/speedEl.value 


const writeText=()=>{
    // separate the single letters in the text
    textEl.innerText=text.slice(0,idx)
    idx++
    // reset idx number in the end of text 
    if(idx>text.length){
        idx=1
    }
    // call this function for next letter
    // This duration will 
    // setTimeout(writeText,500)
    setTimeout(writeText,speed)
}
// fire the function for the first time
writeText()

// speed controlling function
speedEl.addEventListener('input',(e)=>{speed=300/e.target.value})