const container=document.querySelector('.container')
const unsplashURL='https://source.unsplash.com/random/'
const rows =10

// a funtion which makes <img > tags
for(let i=0; i<rows*3; i++){
    const img=document.createElement('img')
    img.src=`${unsplashURL}${getRandomSize()}`

    container.appendChild(img)
}

//  a function which generates the random size
function getRandomSize(){
    return `${getRandomNr()}x${getRandomNr()}`
}

// return a random number between 300 and 310
function getRandomNr(){
    return Math.floor(Math.random()*10)+300
}
/**
 * Things to learn from thsi Project:
 * how to create an image from js and add the source to them 
 */