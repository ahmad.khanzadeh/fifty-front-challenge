const addBtn=document.getElementById('add')
// get former notes from loca storage
// it must be located in top of the code (to prevent hierarcy issue)
const notes=JSON.parse(localStorage.getItem('notes'))
/* 
    Attention : 
    if this application is for a permanent use:
    change the 'localStorage' with 'sessionStorage'
    in that case informations will be saved in the system
    eventhough user close the browser 
*/

addBtn.addEventListener('click',()=>addNewNote())

function addNewNote( text=''){
    const note=document.createElement('div')
    note.classList.add('note')

    note.innerHTML=`
    <div class="tools">
        <button class="edit"><i class="fas fa-edit"></i></button>
        <button class="delete"><i class="fas fa-trash-alt"></i></button>
    </div>
    <div class="main ${text ? "": "hidden"}"></div>
    <textarea class="${text ? "hidden": ""}"></textarea>

    `
    const editBtn=note.querySelector('.edit')
    const deleteBtn=note.querySelector('.delete')
    const mainBtn=note.querySelector('.main')
    const textArea=note.querySelector('textarea')

    // for deleting note
    deleteBtn.addEventListener('click',()=>{
        note.remove()
        // a text is removed from the front so call the following function to overwrite new text in local storare 
        updateLocalStorage()
    })
    // for hidding textarea and main
    editBtn.addEventListener('click',()=>{
        mainBtn.classList.toggle('hidden')
        textArea.classList.toggle('hidden')
    }) 
    // put reciving text(from where function is called) in textarea and in main section
    textArea.value=text
    /* using marked librery to use symbols in text
    //Attention:
    // in case the internet is disconnected, the following line will not work.
     please uncomment the following line for online condition */
    // mainBtn.innerHTML=marked(text)
    mainBtn.innerHTML=text


    textArea.addEventListener('input',(e)=>{
        const {value} =e.target

        mainBtn.innerHTML=value
        // now sava data in local storage by means of a function in line 57
        updateLocalStorage()
    })

    document.body.appendChild(note)
}

// to save data in local storage:
/* 
localStorage.setItem('Esm',JSON.stringify({name:'Ahmad', family:'Khanzadeh'}))
JSON.parse(localStorage.getItem('Esm'))
localStorage.removeItem('Esm')
*/

function updateLocalStorage(){
    const allNotesText=document.querySelectorAll('textarea')
    const allNotes=[]

    allNotesText.forEach(singleNote=>allNotes.push(singleNote.value))

    localStorage.setItem('notes',JSON.stringify(allNotes))
}


// if any notes where existed, make new cards from the saved information 
if(notes){
    notes.forEach(note => addNewNote(note))
}
