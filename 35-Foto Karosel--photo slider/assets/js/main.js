const imgs=document.getElementById('imgs')
const leftBtn=document.getElementById('left')
const rightBtn=document.getElementById('right')

const img=document.querySelectorAll('#imgs img')

let idx=0

let localInterval=setInterval(runTheImages, 2000)

// this function will change image each 2 sec automatically
// it will call the change image function each 2 sec
function runTheImages(){
    idx++
    changeTheImage()
}

// image must be transform:translate(500px) so next image will be appers
// style will be added to class .image-container 
function changeTheImage(){

    // in case index is longet than images lenght, reset the index
    if(idx>img.length -1){
        idx=0
    }else if(idx <0){
        // in case user pressed previus image, display the last image
        idx=img.length -1
    }
    // because next and former images are 500px away from current imge
    // 
    imgs.style.transform=`translateX(${-idx *500}px)`
}

/**Adding functionality to buttons */
rightBtn.addEventListener('click',()=>{
    idx++
    changeTheImage()
    // start counting time again
    resetInterval()
})
leftBtn.addEventListener('click',()=>{
    idx--
    changeTheImage()
    // start counting time again
    resetInterval()
})

/**to clear the former interaval and counting the duration again,
 * following function will clear the former interval and start it again
 */
function resetInterval(){
    clearInterval(localInterval);
    localInterval=setInterval(runTheImages,2000)
}
