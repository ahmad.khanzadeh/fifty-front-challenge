const poke_container=document.getElementById('poke-container')
const pokemon_count=150
const colors={
    fire:'#FDDFD',
    grass:"#DEFDE0",
    electric:'#FCF7DE',
    water:'#DEF3FD',
    ground:'#f4e7da',
    rock:'#d4d5d4',
    fairy:'#fceaff',
    poison:'#98d7a5',
    bug:'#f8d5a3',
    dragon:'#97b3e6',
    psychic:'#eaeda1',
    flying:'#f5f5f5',
    flighting:'#e6e0d4',
    normal:'#f5f5f5'
}

//turn all upper colors to an object to use in createPokemonCard function
const main_types=Object.keys(colors)

// to fetch data from api==> make an async arrow function
const fetchPokemons= async ()=>{
    // do it as long as we have a pokemon
    for(let i=1; i<=pokemon_count; i++){
        // will return a promise, so add await to the function
            await getPokemon(i)
    }
}
const getPokemon = async(id) =>{
    const url=`https://pokeapi.co/api/v2/pokemon/${id}`
    // fetch data from this url
    const res=await fetch(url)
    // turn data to a json format
    const data=await res.json()
    createPokemonCard(data)
}

// make a function which create cards from recived data
const createPokemonCard=(pokemon)=>{
    const pokemonEl=document.createElement('div')
    pokemonEl.classList.add('pokemon')
    // change recived name to upper case---> we just make the first element to uppercase
    const localName=pokemon.name[0].toUpperCase()+pokemon.name.slice(1)
    // change the recived id to a string
    const localId=pokemon.id.toString().padStart(3, '0')
    // to get type  console.log(pokemon.type) ---> an array -> make a map
    const poke_types=pokemon.types.map(type => type.type.name)
    // in case it was not negative, select a type from constant types in upper lineks
    const localType=main_types.find(type => poke_types.indexOf(type) > -1)
    // select the color from array list 
    const localColor=colors[localType]
    // use this color as background color
    pokemonEl.style.backgroundColor=localColor

    const pokemonInnerHTML=`
    <div class="pokemon img-container" style="background-color:rgb(222,253,224)">
         <img src="https://pokeres.bastionbot.org/images/pokemon/${pokemin.id}.png" alt="pokemon-image">
    </div>
    <div class="info">
        <span class="nember">${localId}</span>
        <h3 class="name">${localName}</h3>
        <small class="type"> Type: <span>${localType}</span></small>
    </div>
    `
    pokemonEl.innerHTML=pokemonInnerHTML
    poke_container.appendChild(pokemonEl)
}

// call the main function
fetchPokemons()

/**
 * learned from challanges: 
 * 1_ how to fetch data from a url: line 24 to line 38
 * 2_how to turn first element to uppercase: line 45
 * 3_change the recived id to displayable string: line 47
 * 4_select a color base on the recived nam( hera recived type) line 53 and line 3
 * 
 */