const content=document.querySelectorAll('.content')
const listItem=document.querySelectorAll('nav ul li')

listItem.forEach((singleItem, idx)=>{
    singleItem.addEventListener('click',()=>{
        hideAllContents()
        hideAllItems()

        singleItem.classList.add('active')
        content[idx].classList.add('show')
        
    })
})

function hideAllContents(){
    content.forEach(content => content.classList.remove('show'))
}
function hideAllItems(){
    listItem.forEach(item => item.classList.remove('active'))
}