const toggles=document.querySelectorAll('.faq-toggle');
// in click of every single button, the class 'active' will be added to 
// parent element. ( it will make )

toggles.forEach(toggle =>{
    toggle.addEventListener('click', () =>{
        toggle.parentNode.classList.toggle('active');
    })
})