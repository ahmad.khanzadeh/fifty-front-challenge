const btn=document.getElementById('searchBtn');
const searchBar=document.getElementById('searchInput');
const searchContainer=document.querySelector('.search');

btn.addEventListener('click',()=>{
    searchContainer.classList.toggle('active');
    searchBar.focus();
})