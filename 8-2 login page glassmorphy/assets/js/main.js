const pass_field=document.querySelector('.password');
const show_btn=document.querySelector('.show');

show_btn.addEventListener('click',()=>{
    if(pass_field.type==='password'){
        pass_field.type="text";
        pass_field.style.color="#3498db";
        show_btn.textContent="hide";
    }else{
        pass_field.type="password";
        pass_field.style.color="#222";
        show_btn.textContent="show";
    }
});