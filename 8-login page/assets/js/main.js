const labels=document.querySelectorAll('.form-control label');


labels.forEach(label =>{
    label.innerHTML=label.innerText
    .split('')
    .map((letter,idx)=>`<span style="transition-delay:${idx *50}ms">${letter}</span>`)
    .join('')
    // line 5 will split all single letters into separated char
    // line 6 will put every single letter in a <span> - we could do anything else with it
    // line 7 will join all separated span to a single unit
    // attention:
    // in .map( ) function, do not use { } to define your tasks. It will break the code 
})
