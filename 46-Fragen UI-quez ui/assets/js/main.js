const quizData=[
    {
        question:'Welchem Sprachen functioniert im ein Webbrowser(z.B: Chrom)?',
        a:'Java',
        b:'C++',
        c:'Python',
        d:'Javascript',
        correct:'d',
    },
    {
        question:'Was ist ,HTML" Abkurzun fur?',
        a:'Hybertext Markup Language',
        b:'Hybertext Markdown Language',
        c:'Hybertext Machin Language',
        d:'Helicopters Terminals Motorboats Lamborginis',
        correct:'b',
    },
    {
        question:'Was ist ,CSS" Abkurzun fur?',
        a:'Centeral Style Sheet',
        b:'Cascading Style Sheets',
        c:'Cascading Simple Sheets',
        d:'Cars SUVs Sailboats',
        correct:'b',
    },
    {
        question:'Im welcher Jahre hast Javascript gegrundet?',
        a:'1996',
        b:'1995',
        c:'1994',
        d:'Die Antwort is nicht da',
        correct:'b',
    },
];

const quiz = document.getElementById('quiz')
const answerEls=document.querySelectorAll('.answer')
const questionEl=document.getElementById('question')
const a_text=document.getElementById('a_text')
const b_text=document.getElementById('b_text')
const c_text=document.getElementById('c_text')
const d_text=document.getElementById('d_text')
const submitBtn=document.getElementById('submit')

let currentQuiz=0
let score=0

// the function which run the application 
loadQuiz()

function loadQuiz(){
    // the former answers should be removed
    deselectFormerAnswers()
    // read the current question
    const currentQuizData=quizData[currentQuiz]
    // set the current quiz data to different part of the element
    questionEl.innerText=currentQuizData.question
    a_text.innerText=currentQuizData.a
    b_text.innerText=currentQuizData.b
    c_text.innerText=currentQuizData.c
    d_text.innerText=currentQuizData.d
}

// the functin which cleans the inputs form 
function deselectFormerAnswers(){
    // uncheck all answers elements
    answerEls.forEach(answerEl => answerEl.checked = false)
}

// a function which display which item has the checked property( which is selected)
function getSelectedItem(){
    let answer
    answerEls.forEach(answerEl =>{
        if(answerEl.checked){
            answer= answerEl.id
        }
    })

    return answer
}

// the function which check the answers
submitBtn.addEventListener('click', ()=>{
    // a function which findout which item was selected
    const answer=getSelectedItem()
    // chenck the selected answer with answer in the data
    if(answer){
        if(answer === quizData[currentQuiz].correct){
            // add 1 point to the score
            score++
        }
        // go to next question ( even if the answer is wronge)
        currentQuiz++
        // in case there are still some question in quizData, recall the "loadQuiz()" function to display next question
        if(currentQuiz < quizData.length){
            loadQuiz()
        }else{
            // display the score and make a button to have the exam again
            quiz.innerHTML=`
            <h2>Sie haben ${score}/${quizData.length} Fragen antwortet</h2>
            <button onclick="location.reload()">Haben Sie doch das Test noch einmal!</button>
            `
        }
    }
})


/**
 * Things to learn from this Project
//  1) how to get an array of data and iterate over it 
    2)how to put reciving data in HTML from that big array: line 51
    3)how to uncheck former selected option : line 65
    4)to find out which option is selected: line 71
    5)run the quiz and collect data from each quiz: line 83
 */