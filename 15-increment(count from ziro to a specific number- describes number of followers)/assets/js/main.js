const counters=document.querySelectorAll('.counter');

counters.forEach(counter=>{
    counter.innerText='0';

    // the displayed number will be rcived from data-target
    const updateCounter=()=>{
        const target=Number(counter.getAttribute('data-target'));
        // console.log(typeof target, target);
        const c= Number(counter.innerText);

        // to count increment steps
        const increment=target/300;

        // console.log(increment);
        if(c<target){
            // increase the displayed number by adding the increment to innerText
            counter.innerText=`${Math.ceil(c+ increment)}`;

            setTimeout(updateCounter,1);
        }else{
            // stop the procidure in last number
            counter.innerText=target;
        }
    }
    updateCounter();
})