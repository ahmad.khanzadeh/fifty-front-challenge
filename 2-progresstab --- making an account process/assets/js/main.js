// move to next and preview buttons
const progress=document.getElementById('progress');
const pre=document.getElementById('pre');
const next=document.getElementById('next');
const circles=document.querySelectorAll('.circle');

let currentActive=1;

next.addEventListener('click', () =>{
    currentActive++;
    if(currentActive > circles.length){
        currentActive=circles.length;
    }
    update()
   
})
pre.addEventListener('click',()=>{
    currentActive--;

    if(currentActive<=1){
        currentActive=1;
    }

    update()
})

function update(){
    circles.forEach((singleCircle,idx)=>{
        if(idx<currentActive){
            singleCircle.classList.add('active')
        }else{
            singleCircle.classList.remove('active')
        }
    })
    const activeCircles=document.querySelectorAll('.active');
    // the proportion of activeCirles.length/circles.length will make the width of progress bar 
    // console.log(activeCircles.length, circles.length);
    progress.style.width=((activeCircles.length-1)/(circles.length-1))*100 + '%';

    // enable the prev button in case of first circle
    if(currentActive===1){
        pre.disabled=true;
        // pre.disabled=false;
    }else if(currentActive===circles.length){
        next.disabled=true;
        // pre.disabled=false;
    }else{
        pre.disabled=false;
        next.disabled=false;
    }
}
