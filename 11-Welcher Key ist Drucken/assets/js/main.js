const insert=document.getElementById('insert');

window.addEventListener('keypress',(e)=>{
    // all key data are located in event 
    console.log(e.key);
    console.log(e.code);
    console.log(e.keyCode);
    

    // insert all of the content by this 
    // to fix the null display in case of pressing "space" the following inline if was writed
    insert.innerHTML=`
    <div class="key">
    
        ${e.key ===' ' ? 'Space' : e.key} 
        <small> event.key</small>
    </div>
    <div class="key">
        ${e.code}
         <small>event.keyCode</small>
    </div>
    <div class="key">
        ${e.keyCode}
         <small>event.code </small>
    </div>
    <div class="key">
         Press any key to get the keyCode and code 
    </div>
    `
})