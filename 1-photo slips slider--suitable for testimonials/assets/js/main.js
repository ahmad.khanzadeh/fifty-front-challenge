const panels=document.querySelectorAll('.panel');

panels.forEach((panel)=>{
     panel.addEventListener('click',()=>{
        // to remove other active classes-- discribed in line 9
        removeActiveClasses()
        panel.classList.add('active')
     })
});

function removeActiveClasses(){
    panels.forEach((panel)=>{
        panel.classList.remove('active');
    })
}