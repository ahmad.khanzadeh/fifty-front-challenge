const ahmad=document.querySelector
const sliderContainer=document.querySelector('.slider-container')
const sliderRight=document.querySelector('.right-slide')
const slideLeft=document.querySelector('.left-slide')
const upButton=document.querySelector('.up-button')
const downButton=document.querySelector('.down-button')
const slidesLength=sliderRight.querySelectorAll('div').length


let activeSlideIndex=0;

// to push the left slide up so new slide is visible 
slideLeft.style.top=`-${(slidesLength -1)*100}vh`

// adding event listener to all buttons 
upButton.addEventListener('click',()=>changeSlide('up'))
downButton.addEventListener('click',()=>changeSlide('down'))
// the 'up' and 'down' events will controll the 'if' statement in changeSlide function


const changeSlide=(direction)=>{
    // get the user height
    const sliderHeight=sliderContainer.clientHeight
    //if user pressed up arrow:  increase the index number of slide
    if(direction==='up'){
        activeSlideIndex++
        if(activeSlideIndex > slidesLength -1 ){
            activeSlideIndex = 0
        }
    }
    // if user pressed down arrow: decrease the index number of slide 
    if(direction==='down'){
        activeSlideIndex--
        if(activeSlideIndex < 0 ){
            activeSlideIndex = slidesLength -1
        }
    }
    // add this changed number to right slides(image)
    sliderRight.style.transform=`translateY(-${(activeSlideIndex*sliderHeight)}px)`
    // add this change number to left slides( Texts)
    slideLeft.style.transform=`translateY(${(activeSlideIndex*sliderHeight)}px)`
    
}